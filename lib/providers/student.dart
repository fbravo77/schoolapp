import 'dart:convert';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;

class TempCourse {
  String name;
  double score;

  TempCourse({this.name, this.score});

  factory TempCourse.fromJson(Map<String, dynamic> json) {
    return TempCourse(name: json['courseName'], score: json['courseNote']);
  }
}

class Student with ChangeNotifier {
  int id;
  String name;
  double averageScore;
  String imageUrl;
  int pendingHomeworks;
  int unreadMessages;
  List<TempCourse> courseNotes;

  fromJson(Map<String, dynamic> json) {
    var currentListCourse = json['courseNotes'] as List;

    List<TempCourse> courseList =
        currentListCourse.map((i) => TempCourse.fromJson(i)).toList();

    id = json['id'];
    name = json['completeName'];
    averageScore = json['average'];
    pendingHomeworks = json['pendingHomeworks'];
    unreadMessages = json['unreadMessages'];
    courseNotes = courseList;
  }

  Future<void> fetchAndSetStudent() async {
    
    try {
      final response = await http.get(Uri.parse('http://10.0.2.2:5000/student/out/1'), headers: {
        HttpHeaders.authorizationHeader: "Bearer " +
            "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhbnRvbnlicmF2bzc3OEBnbWFpbC5jb20iLCJpYXQiOjE2MDgwNTM0MDksImV4cCI6MTYzOTU4OTQwOX0.fBdeZ0MTsq1Esul4WVHCZnV_CHqWbTfgFRKmtFGVHZFhi_YxDIGwXSswOdza_gVTKeLth6rNcROCf1s9SOAGuw"
      });
      print("CONSULTANDO ESTUDIANTE");
      if (response.statusCode == 200) {
        final extractedData =
            json.decode(response.body) as Map<String, dynamic>;

        fromJson(extractedData);
        notifyListeners();
      } else {
        print(response.statusCode);
        throw (response.statusCode);
      }
    } catch (error) {
      throw (error);
    }
  }
}
