import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class ScoreDetail {
  String name;
  double score;

  ScoreDetail({this.name, this.score});

  factory ScoreDetail.fromJson(Map<String, dynamic> json) {
    return ScoreDetail(
        name: json['homeworkName'], score: json['homeworkScore']);
  }
}

class Course {
  final int id;
  final String courseName;
  final double courseScore;
  final double accumulatedScore;
  final int pendingHomework;
  List<ScoreDetail> courseNotes;

  Course(
      {this.id,
      this.courseName,
      this.courseScore,
      this.accumulatedScore,
      this.pendingHomework,
      this.courseNotes});

  factory Course.fromJson(Map<String, dynamic> json) {
    var currentListCourseNotes = json['courseNotesDetail'] as List;

    List<ScoreDetail> courseListNotes =
        currentListCourseNotes.map((i) => ScoreDetail.fromJson(i)).toList();

    return Course(
        id: json['id'],
        courseName: json['name'],
        accumulatedScore: json['accumulatedScore'],
        pendingHomework: json['pendingHomework'],
        courseScore: json['score'],
        courseNotes: courseListNotes);
  }
}

class Courses with ChangeNotifier {
  List<Course> courses;

  fromJson(List<dynamic> json) {
    List<Course> currentCourses = new List<Course>();

    currentCourses = json.map((i) => Course.fromJson(i)).toList();
    print(currentCourses.elementAt(0).courseNotes.elementAt(0).name);

    courses = currentCourses;
  }

  Future<void> fetchCourses() async {
    
    try {
      final response = await http.get(Uri.parse('http://10.0.2.2:8080/course/student-courses/1'), headers: {
        HttpHeaders.authorizationHeader: "Bearer " +
            "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhbnRvbnlicmF2bzc3OEBnbWFpbC5jb20iLCJpYXQiOjE2MDgwNTM0MDksImV4cCI6MTYzOTU4OTQwOX0.fBdeZ0MTsq1Esul4WVHCZnV_CHqWbTfgFRKmtFGVHZFhi_YxDIGwXSswOdza_gVTKeLth6rNcROCf1s9SOAGuw"
      });
      print("CONSULTANDO CURSOS");
      if (response.statusCode == 200) {
        fromJson(json.decode(response.body) as List);
        notifyListeners();
      } else {
        throw Exception('Failed to load message');
      }
    } catch (error) {
      throw (error);
    }
  }
}
