import 'package:educapp/providers/student.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'text_subtitle.dart';

class PendingHomework extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    {
      return Row(
        children: <Widget>[
          Container(
            padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
            child: TextSubtitle("Tareas Pendientes"),
          ),
          Spacer(),
          Container(
            padding: const EdgeInsets.only(left: 20.0, right: 20),
            child: Consumer<Student>(builder: (ctx, orderData, child) {
              return TextSubtitle(orderData.pendingHomeworks.toString());
            }),
          ),
        ],
      );
    }
  }
}
