import 'package:educapp/providers/student.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AverageStudent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0),
      child: Consumer<Student>(builder: (ctx, orderData, child) {
        return Column(
          children: <Widget>[
            SizedBox(height: 20.0),
            ExpansionTile(
              title: orderData.averageScore > 61
                  ? Text(orderData.averageScore.toString(),
                      style: TextStyle(
                          fontSize: 18.0, fontWeight: FontWeight.bold))
                  : Text(
                      orderData.averageScore.toString(),
                      style: TextStyle(
                          fontSize: 18.0,
                          fontWeight: FontWeight.bold,
                          color: Colors.red),
                    ),
              leading: Text("Promedio:",
                  style:
                      TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold)),
              children: [
                for (var courseNote in orderData.courseNotes)
                  ListTile(
                    title: Text("${courseNote.name} :"),
                    trailing: Text("${courseNote.score}"),
                  ),
              ],
            ),
          ],
        );
      }),
    );
  }
}
