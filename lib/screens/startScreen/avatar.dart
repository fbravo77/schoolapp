import 'package:flutter/material.dart';

class Avatar extends StatelessWidget {
  final double mediaQuery;

  Avatar(this.mediaQuery);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      padding: const EdgeInsets.all(30),
      child: CircleAvatar(
        radius: mediaQuery * 0.3,
        backgroundImage:
            NetworkImage('https://picsum.photos/id/1074/5472/3648'),
        backgroundColor: Colors.transparent,
      ),
    );
  }
}
