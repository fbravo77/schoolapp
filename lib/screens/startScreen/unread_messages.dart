import 'package:educapp/providers/student.dart';
import 'package:educapp/routes/Routes.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'text_subtitle.dart';

class UnreadMessages extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => {Navigator.pushNamed(context, Routes.reportsScreenRoute)},
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
              padding: const EdgeInsets.all(20),
              child: TextSubtitle("Mensajes")),
          Container(
              padding: const EdgeInsets.only(left: 20.0, right: 20),
              child: Consumer<Student>(builder: (ctx, orderData, child) {
                return TextSubtitle(orderData.unreadMessages.toString());
              })),
        ],
      ),
    );
  }
}
