import 'package:educapp/providers/student.dart';
import 'package:educapp/screens/startScreen/pending_homework.dart';
import 'package:educapp/screens/startScreen/student_name.dart';
import 'package:educapp/screens/startScreen/unread_messages.dart';
import 'package:educapp/utils/server_error.dart';
import 'package:flutter/material.dart';

import '../../utils/my_drawer.dart';
import 'avatar.dart';
import 'package:provider/provider.dart';

import 'average_student.dart';

class StartScreen extends StatefulWidget {
  static const String routeName = '/start-screen';

  @override
  _StartScreenState createState() => _StartScreenState();
}

class _StartScreenState extends State<StartScreen> {
  Future<void> studentFuture;

  @override
  void initState() {
    studentFuture =
        Provider.of<Student>(context, listen: false).fetchAndSetStudent();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context).size;

    return Scaffold(
        appBar: AppBar(
          title: const Text("Inicio"),
          elevation: 5,
          centerTitle: true,
        ),
        drawer: MyDrawer(),
        body: FutureBuilder<void>(
            future: studentFuture,
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.done &&
                  !snapshot.hasError) {
                return SingleChildScrollView(
                    child: Column(children: <Widget>[
                  Avatar(mediaQuery.width),
                  Card(
                    margin: EdgeInsets.all(15),
                    elevation: 10,
                    child: Column(children: <Widget>[
                      StudentName(),
                      AverageStudent(),
                      PendingHomework(),
                      UnreadMessages(),
                    ]),
                  )
                ]));
              } else if (snapshot.hasError) {
                print(snapshot.error);
                return ServerError();
              } else {
                return Center(child: CircularProgressIndicator());
              }
            }));
  }
}
