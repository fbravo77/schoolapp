import 'package:flutter/material.dart';

class TextSubtitle extends StatelessWidget {
  final String subtitle;

  TextSubtitle(this.subtitle);

  @override
  Widget build(BuildContext context) {
    return Text(
      subtitle,
      style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
    );
  }
}
