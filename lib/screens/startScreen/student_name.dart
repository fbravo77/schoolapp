import 'package:educapp/providers/student.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class StudentName extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.only(top: 20, bottom: 10),
        child: Consumer<Student>(builder: (ctx, orderData, child) {
          return Text(
            orderData.name,
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
          );
        }));
  }
}
