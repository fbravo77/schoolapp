import 'package:educapp/screens/coursesScreen/acumulated_score.dart';
import 'package:educapp/screens/coursesScreen/list_courses.dart';
import 'package:educapp/screens/startScreen/pending_homework.dart';
import 'package:educapp/utils/server_error.dart';

import '../../providers/courses.dart';
import 'package:educapp/screens/coursesScreen/image_course.dart';

import '../../utils/my_drawer.dart';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CoursesScreen extends StatefulWidget {
  static const String routeName = '/courses-screen';

  @override
  _CoursesScreenState createState() => _CoursesScreenState();
}

class _CoursesScreenState extends State<CoursesScreen> {
  Future<void> coursesFuture;

  @override
  void initState() {
    coursesFuture = Provider.of<Courses>(context, listen: false).fetchCourses();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Cursos"),
          elevation: 5,
          centerTitle: true,
        ),
        drawer: MyDrawer(),
        body: FutureBuilder(
            future: coursesFuture,
            builder: (ctx, dataSnapshot) {
              if (dataSnapshot.hasError) {
                print(dataSnapshot.error);
                return ServerError();
              } else if (dataSnapshot.connectionState == ConnectionState.done &&
                  !dataSnapshot.hasError) {
                return ListCourses();
              }
              return Center(
                child: CircularProgressIndicator(),
              );
            }));
  }
}
