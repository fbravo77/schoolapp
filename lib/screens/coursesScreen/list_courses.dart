import 'package:educapp/providers/courses.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'acumulated_score.dart';
import 'image_course.dart';
import 'pending_homework.dart';

class ListCourses extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<Courses>(builder: (ctx, orderData, child) {
      return ListView.builder(
          itemCount: orderData.courses.length,
          itemBuilder: (BuildContext context, int index) {
            return Card(
              margin: EdgeInsets.all(15),
              elevation: 5,
              child: Column(
                children: <Widget>[
                  ImageCourse(orderData.courses.elementAt(index).courseName),
                  SizedBox(
                    height: 10,
                  ),
                  AcumulatedScore(orderData.courses.elementAt(index)),
                  PendingHomework(),
                ],
              ),
            );
          });
    });
  }
}
