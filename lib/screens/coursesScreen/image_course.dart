import 'package:educapp/providers/courses.dart';
import 'package:educapp/screens/coursesScreen/title_course.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ImageCourse extends StatelessWidget {
  final String courseName;
  ImageCourse(this.courseName);

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context).size;
    return Consumer<Courses>(builder: (ctx, orderData, child) {
      return Stack(
        children: <Widget>[
          Image.asset(
            "assets/images/logo.png",
            fit: BoxFit.fitWidth,
            height: mediaQuery.height * 0.2,
            width: mediaQuery.width,
          ),
          TitleCourse("$courseName"),
        ],
      );
    });

    /**/
  }
}
