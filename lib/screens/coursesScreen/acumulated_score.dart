import 'package:educapp/providers/courses.dart';
import 'package:flutter/material.dart';

class AcumulatedScore extends StatelessWidget {
  final Course course;
  AcumulatedScore(this.course);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        ExpansionTile(
          title: Text(
              "Puntos: ${course.courseScore} / ${course.accumulatedScore}",
              style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold)),
          children: [
            for (var courseNote in course.courseNotes)
              ListTile(
                title: Text("${courseNote.name} :"),
                trailing: Text("${courseNote.score}"),
              ),
          ],
        ),
      ],
    );
  }
}
