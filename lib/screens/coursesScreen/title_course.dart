import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class TitleCourse extends StatelessWidget {
  final String title;

  TitleCourse(this.title);

  @override
  Widget build(BuildContext context) {
    return Text(
      title,
      style: GoogleFonts.josefinSans(
          fontSize: 35, backgroundColor: Colors.black54, color: Colors.white),
    );
  }
}
