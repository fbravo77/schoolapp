import 'package:educapp/routes/Routes.dart';
import 'package:educapp/screens/startScreen/text_subtitle.dart';
import 'package:flutter/material.dart';

class PendingHomework extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Container(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: TextSubtitle("Tareas Pendientes")),
        Spacer(),
        Container(
          padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
          child: TextSubtitle("3"),
        ),
        Padding(
          padding: const EdgeInsets.only(right: 20),
          child: RaisedButton(
            onPressed: () {
              Navigator.pushNamed(context, Routes.homeworkScreenRoute);
            },
            child: Text("Ver", style: TextStyle(color: Colors.white)),
            color: Theme.of(context).accentColor,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
          ),
        ),
      ],
    );
  }
}
