import 'package:flutter/material.dart';

class HomeworkScreen extends StatelessWidget {
  static const String routeName = '/homework-screen';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Tareas"),
          elevation: 5,
          centerTitle: true,
        ),
        body: SingleChildScrollView(
          child: Card(
            margin: EdgeInsets.all(15),
            elevation: 10,
            child: Padding(
              padding: const EdgeInsets.all(14.0),
              child: Column(children: <Widget>[
                Container(
                  child: Text(
                    "1) Matematica",
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 35),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                    alignment: Alignment.topLeft,
                    child: Text(
                      "Fecha: 05/05/2020",
                      style: TextStyle(
                          fontSize: 25,
                          color: Colors.deepOrange,
                          fontWeight: FontWeight.w500),
                    )),
                Container(
                    alignment: Alignment.topLeft,
                    child: Text(
                      "Punteo: 10 pts",
                      style: TextStyle(
                          fontSize: 25,
                          color: Colors.deepOrange,
                          fontWeight: FontWeight.w500),
                    )),
                SizedBox(
                  height: 10,
                ),
                Container(
                    alignment: Alignment.topLeft,
                    child: Text(
                      "Detalle: ",
                      style: TextStyle(
                          fontSize: 30,
                          color: Colors.deepOrange,
                          fontWeight: FontWeight.w500),
                    )),
                SizedBox(
                  height: 10,
                ),
                Container(
                    alignment: Alignment.topLeft,
                    child: Text(
                      "Realizar la multiplicacion de 6  asdnsakdljsadlkjsadlksajdklsajdlksajdsalkdjsalkdjsakldsjadlksajdlksajdlksa" +
                          "asd" +
                          "asdsadsadsadsadsad sadsadsadjsadjsakldjsakldsajdklsa ausidhsaudihsaui asdsaiodusadiosa dsadsandkjsan asdusahdiusa saduhsasabdjb" +
                          "asdjksadjska asjdhsa asdsa" +
                          "dsa dsadsadsadsadsa",
                      style: TextStyle(
                        fontSize: 25,
                      ),
                    )),
                SizedBox(height: 10),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                        alignment: Alignment.topLeft,
                        child: Text(
                          "Imagenes:",
                          style: TextStyle(
                            fontSize: 30,
                          ),
                        )),
                    FlatButton(
                      child: Icon(
                        Icons.expand_more,
                        color: Colors.white,
                      ),
                      color: Colors.deepOrange,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(90)),
                      onPressed: () {},
                    ),
                  ],
                ),
              ]),
            ),
          ),
        ));
  }
}
