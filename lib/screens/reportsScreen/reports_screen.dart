import 'package:educapp/models/messages.dart';
import 'package:educapp/utils/server_error.dart';
import 'package:flutter/material.dart';

import '../../utils/my_drawer.dart';

class ReportsScreen extends StatelessWidget {
  static const String routeName = '/reports-screen';

  final Future<Messages> message;

  ReportsScreen(this.message);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Notificaciones generales"),
        elevation: 5,
        centerTitle: true,
      ),
      drawer: MyDrawer(),
      body: FutureBuilder<Messages>(
        future: message,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return ListView.builder(
                itemCount: snapshot.data.messages.length,
                itemBuilder: (BuildContext context, int index) {
                  return Card(
                    margin: EdgeInsets.all(15),
                    elevation: 5,
                    child: Padding(
                      padding: const EdgeInsets.all(15),
                      child: Column(children: <Widget>[
                        Container(
                            alignment: Alignment.topLeft,
                            child: Text(
                              "De: Junta directiva",
                              style: TextStyle(
                                  fontSize: 25,
                                  color: Colors.deepOrange,
                                  fontWeight: FontWeight.w500),
                            )),
                        Container(
                            alignment: Alignment.topLeft,
                            child: Text(
                              "Asunto: ${snapshot.data.messages.elementAt(index).subject}",
                              style: TextStyle(
                                  fontSize: 25,
                                  color: Colors.deepOrange,
                                  fontWeight: FontWeight.w500),
                            )),
                        Container(
                            alignment: Alignment.topLeft,
                            child: Text(
                              "Fecha: ${snapshot.data.messages.elementAt(index).date}",
                              style: TextStyle(
                                  fontSize: 25,
                                  color: Colors.deepOrange,
                                  fontWeight: FontWeight.w500),
                            )),
                        SizedBox(
                          height: 10,
                        ),
                        Container(
                            alignment: Alignment.topLeft,
                            child: Text(
                              "Mensaje:",
                              style: TextStyle(
                                  fontSize: 25,
                                  color: Colors.deepOrange,
                                  fontWeight: FontWeight.w500),
                            )),
                        SizedBox(
                          height: 10,
                        ),
                        Container(
                            alignment: Alignment.topLeft,
                            child: Text(
                              "${snapshot.data.messages.elementAt(index).message}",
                              style: TextStyle(
                                fontSize: 23,
                              ),
                            )),
                        SizedBox(height: 10),
                      ]),
                    ),
                  );
                });
          } else if (snapshot.hasError) {
            return ServerError();
          }
          // Por defecto, muestra un loading spinner
          return Center(child: CircularProgressIndicator());
        },
      ),
    );
  }
}
