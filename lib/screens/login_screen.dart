import 'package:educapp/routes/Routes.dart';
import 'package:flutter/material.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.
  // Note: This is a `GlobalKey<FormState>`,
  // not a GlobalKey<MyCustomFormState>.
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context).size;
    return Form(
      key: _formKey,
      child: Scaffold(
        body: Container(
          decoration: new BoxDecoration(
              gradient: new LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [
              Color.fromARGB(255, 255, 220, 105),
              Color.fromARGB(255, 242, 68, 5)
            ],
          )),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Flexible(
                child: Image.asset(
                  "assets/images/logo.png",
                  fit: BoxFit.contain,
                  width: mediaQuery.width * 0.8,
                ),
              ),
              Card(
                margin: EdgeInsets.all(15),
                elevation: 10,
                child: Column(children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(left: 20.0, right: 20),
                    child: TextFormField(
                        decoration: InputDecoration(
                          labelText: "Correo Eléctronico",
                          icon: Icon(Icons.email),
                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Por favor ingresa tu correo';
                          }
                          return null;
                        }),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                        left: 20.0, right: 20, bottom: 10),
                    child: TextFormField(
                        obscureText: true,
                        decoration: InputDecoration(
                            labelText: "Contraseña", icon: Icon(Icons.lock)),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Por favor ingresa tu contraseña';
                          }
                          return null;
                        }),
                  ),
                  FlatButton(
                      onPressed: () => print("HOLAS"),
                      child: Text("Olvidaste tu contraseña?")),
                  RaisedButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30)),
                    color: Theme.of(context).primaryColor,
                    textColor: Colors.white,
                    onPressed: () {
                      Navigator.pushReplacementNamed(
                          context, Routes.startScreenRoute);
                    },
                    child: const Text('Ingresar'),
                  ),
                ]),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
