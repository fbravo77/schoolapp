import 'dart:io';

import 'package:http/http.dart' as http;
import 'dart:convert';

class Message {
  final String subject;
  final String message;
  final String date;

  Message({this.subject, this.message, this.date});

  factory Message.fromJson(Map<String, dynamic> json) {
    return Message(
      subject: json['subject'],
      message: json['message'],
      date: json['date'],
    );
  }
}

class Messages {
  List<Message> messages;
  Messages(this.messages);

  factory Messages.fromJson(List<dynamic> json) {
    List<Message> messages = new List<Message>();

    messages = json.map((i) => Message.fromJson(i)).toList();

    return new Messages(messages);
  }

  static Future<Messages> fetchMessages() async {
    print("CONSULTANDO MENSAJES");
    final response =
        await http.get(Uri.parse('http://10.0.2.2:8080/general-messages/'), headers: {
      HttpHeaders.authorizationHeader: "Bearer " +
          "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhbnRvbnlicmF2bzc3OEBnbWFpbC5jb20iLCJpYXQiOjE2MDgwNTM0MDksImV4cCI6MTYzOTU4OTQwOX0.fBdeZ0MTsq1Esul4WVHCZnV_CHqWbTfgFRKmtFGVHZFhi_YxDIGwXSswOdza_gVTKeLth6rNcROCf1s9SOAGuw"
    });
    if (response.statusCode == 200) {
      return Messages.fromJson(json.decode(response.body) as List);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load message');
    }
  }
}
