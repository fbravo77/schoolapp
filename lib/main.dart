import 'package:educapp/models/messages.dart';

import './screens/homeworksScreen/homework_screen.dart';
import './screens/notificationScreen/notification_screen.dart';
import './screens/reportsScreen/reports_screen.dart';
import 'package:flutter/material.dart';

import './screens/coursesScreen/courses_screen.dart';
import 'package:provider/provider.dart';

import './screens/login_screen.dart';
import './screens/startScreen/start_screen.dart';
import './routes/Routes.dart';
import 'providers/courses.dart';
import 'providers/student.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(
          value: Courses(),
        ),
        ChangeNotifierProvider(
          create: (context) => Student(),
        ),
      ],
      child: MaterialApp(
          title: 'School App',
          theme: ThemeData(
              backgroundColor:
                  Colors.white, //Color.fromARGB(241, 241, 235, 218),
              scaffoldBackgroundColor:
                  Colors.white, //Color.fromARGB(241, 241, 235, 218),
              cardColor:
                  Colors.blueGrey[100], colorScheme: ColorScheme.fromSwatch(primarySwatch: Colors.blue).copyWith(secondary: Colors.indigo) //Color.fromARGB(255, 255, 242, 242),
              ),
          home: LoginScreen(),
          routes: <String, WidgetBuilder>{
            Routes.startScreenRoute: (BuildContext context) => StartScreen(),
            Routes.coursesScreenRoute: (BuildContext context) =>
                CoursesScreen(),
            Routes.homeworkScreenRoute: (BuildContext context) =>
                HomeworkScreen(),
            Routes.reportsScreenRoute: (BuildContext context) =>
                ReportsScreen(Messages.fetchMessages()),
            Routes.notificationScreenRoute: (BuildContext context) =>
                NotificationScreen(),
          }),
    );
  }
}
