import '../screens/notificationScreen/notification_screen.dart';
import '../screens/reportsScreen/reports_screen.dart';
import '../screens/homeworksScreen/homework_screen.dart';
import '../screens/startScreen/start_screen.dart';
import '../screens/coursesScreen/courses_screen.dart';

class Routes {
  static const String startScreenRoute = StartScreen.routeName;
  static const String coursesScreenRoute = CoursesScreen.routeName;
  static const String homeworkScreenRoute = HomeworkScreen.routeName;
  static const String reportsScreenRoute = ReportsScreen.routeName;
  static const String notificationScreenRoute = NotificationScreen.routeName;
}
