import 'package:flutter/material.dart';

import './../routes/Routes.dart';

class MyDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      elevation: 5,
      child: ListView(
        children: <Widget>[
          DrawerHeader(
            child: const Text(""),
            decoration: BoxDecoration(
              color: Theme.of(context).backgroundColor,
              image: DecorationImage(
                  image: AssetImage("assets/images/logo.png"),
                  fit: BoxFit.contain),
            ),
          ),
          Container(
            color: Theme.of(context).backgroundColor,
            child: ListTile(
              leading: const Icon(
                Icons.dashboard,
                color: Colors.red,
              ),
              title: const Text('Inicio'),
              onTap: () {
                Navigator.pushNamed(context, Routes.startScreenRoute);
              },
            ),
          ),
          ListTile(
            leading: const Icon(
              Icons.collections_bookmark,
              color: Colors.red,
            ),
            title: const Text('Cursos'),
            onTap: () {
              Navigator.pushNamed(context, Routes.coursesScreenRoute);
            },
          ),
          ListTile(
            leading: const Icon(
              Icons.notifications_none,
              color: Colors.red,
            ),
            title: const Text('Mensajes'),
            onTap: () {
              Navigator.pushNamed(context, Routes.reportsScreenRoute);
            },
          ),
          ListTile(
            leading: const Icon(
              Icons.mail,
              color: Colors.red,
            ),
            title: const Text('Contactar'),
            onTap: () {
              Navigator.pushNamed(context, Routes.coursesScreenRoute);
            },
          ),
          ListTile(
            leading: const Icon(
              Icons.exit_to_app,
              color: Colors.red,
            ),
            title: const Text('Salir'),
            onTap: () {
              Navigator.pushNamed(context, Routes.coursesScreenRoute);
            },
          ),
        ],
      ),
    );
  }
}
