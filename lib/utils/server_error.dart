import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ServerError extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        SizedBox(
          height: 26,
        ),
        Text("ERROR CONSULTANDO DATOS",
            style: TextStyle(
              backgroundColor: Colors.black38,
              fontSize: 30,
              color: Colors.white,
            )),
        Expanded(
          child: SvgPicture.asset(
            "assets/images/error.svg",
          ),
        )
      ],
    );
  }
}
